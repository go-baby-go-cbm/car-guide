# Changelog

----------------------------------------------------------------------------------------------------
## v2.3 [2022-06-12 14:00:00]

### Changed
-   Added photo to README file.
-   Reworked repository history so the versioning shifted from vX to vX.Y.


----------------------------------------------------------------------------------------------------
## v2.2 [2022-06-06 17:00:00]

### Changed
-   Fixed typos in the reference vehicle overview README file.
-   Added annotations for dimensions on some of the reference vehicle overview dimension photos.


----------------------------------------------------------------------------------------------------
## v2.1 [2022-06-06 15:00:00]

### Added
-   Formal design files (CAD design using SolidWorks).
-   Bill of materials.
-   Overview of reference vehicle.

### Changed
-   Dropped cross braces between top bar and vertical side pieces.
-   Added two more braces (now four in total) between the vertical side pieces and the base.


----------------------------------------------------------------------------------------------------
## v2.0

### Added
-   Reference photos of the second revision of the car guide.

### Changed
-   Formalized the design.
-   Shifted from the smaller red car to the larger blue car for the design.
-   Changed from all plastic to metal housing with rubber wheels for the casters.


----------------------------------------------------------------------------------------------------
## v1.0

### Notes
-   Initial design completed by Northwestern students.
-   Includes reference photos of the first car guide completed.


