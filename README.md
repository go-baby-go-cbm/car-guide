# Car Guide

![Car Guide](reference-photos/version-2/fully-assembled.jpg)

## Overview

This car guide allows a Go Baby Go car to be steered by someone assisting a child in car (e.g. a 
parent, guardian, or clinician). It is intended to be used when a child is unable to utilize the 
steering wheel to turn the vehicle but is able to drive the car (via the foot pedal, "Go Button", 
joystick, or other means). Essentially, this guide enables a child to drive the car while someone 
else steers the vehicle.

Functionally, the guide is designed so that the underside of the car rests upon the base and the 
wheels are left to spin freely. This enables and encourages the child to still play with the 
steering wheel while not impacting the guide.


## How to Use this Project

Please treat this design as a baseline / stepping stone for your own guide cars. The instructions 
are more of a template than an absolute. For instance, any scrap wood, wood screws, etc. can be 
used to build a guide.

We encourage all to customize their guides via paint, extra grips for handles, etc.


## Materials Included

The following materials are included in this repository:

-   [CAD Files](cad-files)
    -   SolidWorks design files for the car guide.

-   [Docs](docs)
    -   [Reference Vehicle Overview](docs/reference-vehicle-overview)
        -   Folder containing dimensions and reference photos for the primary vehicle used in the 
            current design of the car guide.
    -   [Bill of Materials](docs/BOM.pdf)
        -   Bill of materials (BOM) for building a car guide.
        -   Both PDF and excel sheets are available.
    -   [Build Guide](docs/build-guide.pdf)
        -   **Follow this document to build your own car guide.**
        -   PDF overview of how to build a car guide.
        -   Generated from the CAD design files.

-   [Reference Photos](reference-photos)
    -   [Version 1](reference-photos/version-1)
        -   Photos detailing a completed version 1 of the car guide.
    -   [Version 2](reference-photos/version-2)
        -   Photos detailing a completed version 2 of the car guide.


## Version Information

This is version 2.3 of this guide. Please see the [Changelog](Changelog.md) for a detailed version 
history.


## License

This project and materials are released under the [Unlicense](LICENSE). We provide the materials 
included here as a reference to anyone working in the Go Baby Go space. We hope this serves as a 
stepping stone to improved designs and would love to hear back on any improvements made on what we 
have created here.


