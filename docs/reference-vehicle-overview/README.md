# Target Vehicle

## Details

While this guide may work for many vehicles, the current design targets the following blue vehicle:

![Target Vehicle](wide-view.jpg)


## Car Dimensions

The following key dimensions may help when determining if customization of the provided design 
materials are necessary:

|                                   DIMENSION                                   |  VALUE (INCHES)   | 
| ----------------------------------------------------------------------------- | :---------------: |
| [Car Length](reference-dimension--length.jpg)                                 |       41.00       |
| [Car Width](reference-dimension--width.jpg) - Wheels Straight                 |       22.00       |
| [Car Width](reference-dimension--width.jpg) - Wheels Turned                   |       23.50       |
| [Underside of car to floor](reference-dimension--gap-from-car-to-floor.jpg)   |        4.00       |
| [Wheel Diameter](reference-dimension--wheel-diameter.jpg)                     |        9.25       |
| [Wheel to Wheel Distance](reference-dimension--wheel-to-wheel-distance.jpg)   |       24.00       |

Please use the above as a general reference. No one dimension drives a particular change in the 
design dimensions of the car guide but if your vehicle is significantly different in size, you can 
use these as a reference when determining any dimension changes required for your build.


